const express = require('express');
const router = express.Router();
const courseController = require('../controllers/courseController');
const auth = require('../auth');


// Route for creating a course
router.post('/', auth.verify, (request, response) => {

	const userData = auth.decode(request.headers.authorization);
	console.log(userData);
	console.log(request.headers.authorization);

	if (userData.isAdmin) {

    	courseController.addCourse(request.body).then(resultFromController => response.send(resultFromController));
    } else {

        response.send('Authorized Admin Only');
    }

	
});

// Route for retrieving all the courses
router.get('/all', (request, response) => {

	courseController.getAllCourse().then(resultFromController => response.send(resultFromController));
});


// Route for retrieving all the ACTIVE courses
router.get('/', (request, response) => {

	courseController.getAllActive().then(resultFromController => response.send(resultFromController));
});

//Route for retrieving specific course
router.get('/:courseId', (request, response) => {

	courseController.getCourse(request.params).then(resultFromController => response.send(resultFromController));
})

router.put('/:courseId', auth.verify, (request,response) => {

	const userData = auth.decode(request.headers.authorization)

	courseController.updateCourse(request.params, request.body, userData).then(resultFromController => response.send(resultFromController));
});


// Activity 
// 1. Create a route for archiving a course. The route must use JWT authentication and obtain the course ID from the url.

router.put('/:courseId/archive', auth.verify, (request, response) => {

	console.log(request.params)

	const data = {
		courseId : request.params.courseId,
		payload : auth.decode(request.headers.authorization).isAdmin
	}

	courseController.archiveCourse(data, request.body).then(resultFromController => response.send(resultFromController))
});




module.exports = router;