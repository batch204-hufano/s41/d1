const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require('../auth');


//Route for checking if the user's email already exist in database
router.post('/checkEmail', (request, response) => {

	userController.checkEmailExists(request.body).then(resultFromController => response.send(resultFromController));
});

// Route for User Registration
router.post('/register', (request, response) => {

	userController.registerUser(request.body).then(resultFromController => response.send(resultFromController));
});


// User Authentication
router.post('/login', (request, response) => {
	userController.loginUser(request.body).then(resultFromController => response.send(resultFromController))
});


//Create a /details route that will accept the user’s Id to retrieve the details of a user.
router.post('/details' , auth.verify, (request, response) => {

	const userData = auth.decode(request.headers.authorization)

	userController.getProfile(userData).then(resultFromController => response.send(resultFromController))
});

//Route to enroll user to a course
router.post("/enroll", auth.verify, (request, response) => {


	let data = {
		// User ID and IsAdmin will be retrieved from the request header
		userId: auth.decode(request.headers.authorization).id,
		// Course ID will be retrieved from the request body
		courseId: request.body.courseId,
		isAdmin: auth.decode(request.headers.authorization).isAdmin

	}

	userController.enroll(data).then(resultFromController => response.send(resultFromController));

});


module.exports = router;