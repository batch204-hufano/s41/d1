const User = require('../models/User');
const Course = require('../models/Course');
const bcrypt = require('bcrypt');
const auth = require('../auth')
// Check if email exists
module.exports.checkEmailExists = (requestBody) => {

	return User.find({email: requestBody.email}).then(result => {

		if(result.length > 0) {
			return result
		} else {
			return false
		}
	})
}


// Controller for User Registration
module.exports.registerUser = (requestBody) => {

	let newUser = new User({

		firstName: requestBody.firstName,
		lastName: requestBody.lastName,
		email: requestBody.email,
		mobileNo: requestBody.mobileNo,
		// bcrypt.hashSync(myPlaintextPassword, saltRounds);
		password: bcrypt.hashSync(requestBody.password, 10)
	})

	return newUser.save().then((user, error) => {

		if(error) {
			return false
		} else {
			return true
		}
	})
}


// User Authentication
module.exports.loginUser = (requestBody) => {

	console.log(requestBody)
	return User.findOne({email: requestBody.email}).then(result => {

	console.log({email: requestBody.email})
		if(result == null) {
			return false
		} else {

			const isPasswordCorrect = bcrypt.compareSync(requestBody.password, result.password);


			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
}

// Create a getProfile controller method for retrieving the details of the user:
module.exports.getProfile = (requestBody) => {

	console.log(requestBody)
	return User.findById(requestBody.id).then(result => {

		result.password = "";
		return result

	})
}

//Enroll user to a class
// Async await will be used in enrolling the user because we will need to update 2 separate documents when enrolling a user
module.exports.enroll = async (data) => {

	if (data.isAdmin === true) {
		return false
	} else {

		// Add the course ID in the enrollments array of the user
		// Creates an "isUserUpdated" variable and returns true upon successful update otherwise false
		// Using the "await" keyword will allow the enroll method to complete updating the user before returning a response back to the frontend
		let isUserUpdated = await User.findById(data.userId).then( user => {

			// Adds the courseId in the user's enrollments array
			user.enrollments.push({courseId: data.courseId});

			// Saves the updated user information in the database
			return user.save().then( (user, error) => {

				if(error) {
					return false
				} else {
					return true
				
				}
		
			});
		});


		// Add the user ID in the enrollees array of the course
		// Using the "await" keyword will allow the enroll method to complete updating the course before returning a response back to the frontend
		let isCourseUpdated = await Course.findById(data.courseId).then(course => {

			// Adds the userId in the course's enrollees array
			course.enrollees.push({userId: data.userId})

			// Saves the updated course information in the database
			return course.save().then((course, error) => {

				if(error) {
					return false

				} else {
					
					return true
				}
			});
		});


		// Condition that will check if the user and course documents have been updated
		if(isUserUpdated && isCourseUpdated) {

			// User enrollment successful
			return true

		} else {

			// User enrollment failure
			return false
		}
	}
}
