const Course = require('../models/Course');
const User = require('../models/User')

// Create a New Course
module.exports.addCourse = (requestBody) => {

	let newCourse = new Course ({

		name: requestBody.name,
		description: requestBody.description,
		price: requestBody.price
	})

	return newCourse.save().then((course, error) => {

		if(error) {

			return false
		} else {

			return true
		}
	})
}


// Retrieve all courses
module.exports.getAllCourse = () => {

	return Course.find({}).then (result => {

		return result;
	})
}

// Retrieve ACTIVE course

module.exports.getAllActive = () => {

	return  Course.find({isActive: true}).then (result => {

		return result;
	})
}

// Retrieving a specific course
module.exports.getCourse = (requestParams) => {

	return Course.findById(requestParams.courseId).then(result => {
		return result;
	})
}

/*
// Updating course
module.exports.updateCourse = (requestParams, requestBody, data) => {

	if(data.isAdmin === true) {

		let updatedCourse = {
			name: requestBody.name,
			description: requestBody.description,
			price: requestBody.price
		};

		return Course.findByIdAndUpdate(requestParams.courseId, updateCourse).then((course, error) => {

			if (error) {
				return false
			} else {
				return true
			}
		})
	} else {

		return false
	}
}
*/

// Controller for updating a record
// module.exports.updateCourse = (requestParams, requestBody, data) => {
	
// 	if (data.isAdmin === true) {
// 		let updatedCourse = {
// 			name: requestBody.name,
// 			description: requestBody.description,
// 			price: requestBody.price
// 		}
	
// 		return Course.findByIdAndUpdate(requestParams.courseId, updatedCourse).then((course, error) => {
// 			if (error) {
// 				return false
// 			} else {
// 				return true
// 			}
// 		})

// 	} else {
// 		return false
// 	}
// }

//Updating course
module.exports.updateCourse = (requestParams, requestBody, data) => {

	return User.findById(data.id).then(result => {
		console.log(result)

		if(result.isAdmin === true) {

			// Specify the fields/properties of the document to be updated
			let updatedCourse = {
				name: requestBody.name,
				description: requestBody.description,
				price: requestBody.price
			};

			// Syntax:
						// findByIdAndUpdate(document ID, updatesToBeApplied)
			return Course.findByIdAndUpdate(requestParams.courseId, updatedCourse).then((course, error) => {

				// Course not updated
				if(error) {

					return false

				// Course updated successfully	
				} else {

					return true
				}
			})

		} else {

			return false
		}

	})


}


// Activity 
// 2. Create a controller method for archiving a course obtaining the course ID from the request params and the course information from the request body.

module.exports.archiveCourse = (data, requestBody) => {

	return Course.findById(data.courseId).then(result => {

		if (data.payload === true) {

			let updateActiveField = {
				isActive: requestBody.isActive
			}

			return Course.findByIdAndUpdate(result._id, updateActiveField).then((course, error) => {

					if(error) {
					
						return false
					
					}  else {

						return true
					}
			})
			 
		} else {

			return false
		} 
	})

}

